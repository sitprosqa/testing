package com.example.tester;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText t1=(EditText) findViewById(R.id.user);
        final EditText t2=(EditText) findViewById(R.id.pass);
        Button b=(Button) findViewById(R.id.submit);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user=t1.getText().toString();
                String pass=t2.getText().toString();


                Toast.makeText(MainActivity.this,"Given Username: "+user +" Password: "+pass, Toast.LENGTH_LONG).show();

            }
        });



    }
}
